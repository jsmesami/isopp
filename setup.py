import re

from setuptools import find_packages, setup


def read_requirements(req_file):
    return [
        line for line in re.sub(r"\s*#.*\n", "\n", req_file.read()).splitlines() if line
    ]


with open("requirements/base.txt") as f:
    REQUIREMENTS = read_requirements(f)

with open("README.md") as f:
    LONG_DESCRIPTION = f.read()

setup(
    name="ISOPP",
    version="1.0.0b",
    description="Informačný systém o používaní pesticídov",
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/jsmesami/isopp",
    author="Ondřej Nejedlý",
    author_email="jsmesami@gmail.com",
    license="MIT License",
    packages=find_packages(),
    entry_points={"console_scripts": ["isopp=isopp.manage:main"]},
    python_requires=">=3.8",
    install_requires=REQUIREMENTS,
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        "Environment :: Web Environment",
        "Framework :: Django",
        "Framework :: Django :: 4.1",
        "Development Status :: 4 - Beta",
        "Operating System :: OS Independent",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: Dynamic Content"
        "License :: OSI Approved :: MIT License"
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
    ],
)
