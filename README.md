# ISOPP: Mapping Chemical Spraying

Information system on pesticide use is an online-based system for mediating public
information regarding the application of pesticides in urban and rural areas
of cities and villages.

## Prerequisites

* Python 3.8+
* Postgres 12+
* Node.js 12.16+
* Yarn 1.22+

## Development

    # prepare database
    psql -U postgres postgres -c "CREATE ROLE isopp SUPERUSER"
    psql -U postgres postgres -c "ALTER ROLE isopp LOGIN"
    psql -U postgres postgres -c "CREATE DATABASE isopp OWNER=isopp"
    psql -U isopp isopp -c "CREATE EXTENSION postgis"

    # prepare and activate virtual environment
    python3 -m venv venv
    source venv/bin/activate

    # install backend dependencies
    pip install -r requirements/dev.txt

    # prepare (+edit) and load env variables
    cp .env.dev .env
    source .env

    # alternatively prepare (+edit) local settings overrides
    cp settings/local_example.py local.py

    # populate database
    isopp/manage.py migrate
    isopp/manage.py loaddata isopp/fixtures/groups.json
    isopp/manage.py loaddata isopp/fixtures/districts.json
    isopp/manage.py createsuperuser

    # install frontend dependencies
    yarn

    # run development servers
    honcho start
