# Deployment

ISOPP runs on Google Cloud Run service

* region: europe-west1 (Belgium)
* 1 CPU, 512MiB per instance
* max no. of instances: 2

## Prerequisites

* Docker
* Google Cloud SDK set up with credentials
* cloud_sql_proxy script

## GCR service

I configured Docker for GCR registry

    gcloud auth configure-docker eu.gcr.io --project isopp-285112

I built the docker image

    docker build -t eu.gcr.io/isopp-285112/isopp:latest -f Dockerfile .

Then pushed the image to GCR registry

    docker push eu.gcr.io/isopp-285112/isopp:latest

It created a necessary bucket providing the image to run within GCR.

Then I created new GCR service running the image with following env variables set:

    DJANGO_SECRET_KEY ***
    DJANGO_STATIC_URL: https://storage.googleapis.com/isopp/static/
    DEFAULT_DB_PASSWORD: ***
    DEFAULT_DB_HOST: /cloudsql/datastore-285111:europe-west1:postgresql
    DEFAULT_DB_PORT: 5432
    EMAIL_HOST: smtp.eu.mailgun.org
    EMAIL_PORT: 587
    EMAIL_HOST_USER: postmaster@mg.isopp.sk
    EMAIL_HOST_PASSWORD: ***

Then I was able to use

    make upload-image

to make and upload newer image and

    make deploy-image

to deploy it.

## Database

I use shared database engine in a separate `datastore` project

    datastore-285111:europe-west1:postgresql

With sql-proxy running

    make sql-proxy

I was able to connect to it locally on port `9000`

    psql --host 127.0.0.1 --port 9000 -U postgres

To connect to database from `isopp` GCR env I located the name of Service Account
automatically created for `isopp` GCR service, then assigned it `Cloud SQL Client`
IAM Role from within `datastore` project.

## Static files

I created `isopp` bucket and set it to Access Control "uniform" and Public Access
to "Public to internet" by adding `Storage Object Viewer` permission
to member `allUsers`.

Then I set CORS headers for the bucket (takes some time to have an effect)

    gsutil cors set cors.json gs://isopp

then uploaded static files

    make compile-assets && make upload-static
