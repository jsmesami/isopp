# This Dockerfile is for GCR deployment.
FROM python:3.11.1-slim

RUN apt-get update && apt-get install -qyy \
    -o APT::Install-Recommends=false -o APT::Install-Suggests=false \
    git gdal-bin

ENV APP_HOME /app
ENV PYTHONPATH $APP_HOME
ENV PYTHONUNBUFFERED 1

WORKDIR $APP_HOME
COPY . ./

RUN pip install --no-cache-dir --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

CMD gunicorn -c isopp/gunicorn.py --chdir isopp -b :$PORT isopp.wsgi
