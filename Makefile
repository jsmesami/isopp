#!/usr/bin/env sh

.PHONY: reqs
reqs:
	pip-compile --resolver=backtracking -Uo ./requirements/base.txt ./requirements/base.in
	pip-compile --resolver=backtracking -Uo ./requirements/dev.txt ./requirements/dev.in

.PHONY: fmt
fmt: black isort

.PHONY: black
black:
	black --exclude migrations -l 90 isopp setup.py

.PHONY: isort
isort:
	isort --skip migrations --profile django -e -m 3 -w 90 isopp setup.py

.PHONY: lint
lint:
	flake8 isopp setup.py

.PHONY: make-messages
make-messages:
	isopp/manage.py makemessages -l cs -l sk -i "venv/*"

.PHONY: compile-messages
compile-messages:
	isopp/manage.py compilemessages -i "venv*"

.PHONY: sql-proxy
sql-proxy:
	cloud_sql_proxy -instances datastore-285111:europe-west1:postgresql=tcp:9000

.PHONY: compile-assets
compile-assets:
	yarn && yarn build

.PHONY: upload-static
upload-static:
	isopp/manage.py collectstatic --noinput && gsutil -m rsync -R static/ gs://isopp/static

.PHONY: upload-image
upload-image:
	gcloud builds submit --project isopp-285112 --tag eu.gcr.io/isopp-285112/isopp:latest

.PHONY: deploy-image
deploy-image:
	gcloud run deploy isopp --project isopp-285112 --image eu.gcr.io/isopp-285112/isopp:latest --platform managed

.PHONY: deploy
deploy: compile-messages compile-assets upload-static upload-image deploy-image
