from .base import *

DEBUG = True

ALLOWED_HOSTS = []

INTERNAL_IPS = ["127.0.0.1"]

TEMPLATES[0]["OPTIONS"]["debug"] = True

INSTALLED_APPS = ["debug_toolbar"] + INSTALLED_APPS

MIDDLEWARE = MIDDLEWARE + ["debug_toolbar.middleware.DebugToolbarMiddleware"]

SECURE_SSL_REDIRECT = False
SECURE_BROWSER_XSS_FILTER = False
SESSION_COOKIE_SECURE = CSRF_COOKIE_SECURE = False

# Might be necessary for PostGis installed with Homebrew on a Mac:
# GDAL_LIBRARY_PATH = '/opt/homebrew/opt/gdal/lib/libgdal.dylib'
# GEOS_LIBRARY_PATH = '/opt/homebrew/opt/geos/lib/libgeos_c.dylib'
