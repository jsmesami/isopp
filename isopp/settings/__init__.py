from contextlib import suppress

from .base import *  # noqa

with suppress(ImportError):
    from .local import *  # noqa
