from datetime import timedelta
from pathlib import Path

from django.contrib.messages import constants as messages
from django.urls import reverse_lazy
from django.utils.log import DEFAULT_LOGGING

import isopp
from isopp.utils.env import env

BASE_DIR = Path(isopp.__file__).parent.parent

SECRET_KEY = env("DJANGO_SECRET_KEY")

DEBUG = False

PROJECT_DOMAIN = "isopp.sk"

ALLOWED_HOSTS = [f".{PROJECT_DOMAIN}", f".{PROJECT_DOMAIN}."]


# Application definition

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "bootstrap4",
    "bootstrap_datepicker_plus",
    "leaflet",
    "phonenumber_field",
    "rest_framework",
    "rest_framework_gis",
    "core",
    "accounts",
    "areas",
    "api",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "core.middleware.WwwRedirectMiddleware",
]

ROOT_URLCONF = "isopp.urls"

PROJECT_TEMPLATES_DIR = BASE_DIR / "isopp" / "templates"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [PROJECT_TEMPLATES_DIR],
        "APP_DIRS": True,
        "OPTIONS": {
            "debug": False,
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "isopp.core.context_processors.config",
            ],
        },
    }
]

WSGI_APPLICATION = "isopp.wsgi.application"


# Databases

DATABASES = {
    "default": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "CONN_MAX_AGE": 600,
        "NAME": env("DEFAULT_DB_NAME", "isopp"),
        "USER": env("DEFAULT_DB_USER", "isopp"),
        "PASSWORD": env("DEFAULT_DB_PASSWORD", ""),
        "HOST": env("DEFAULT_DB_HOST", ""),
        "PORT": env("DEFAULT_DB_PORT", ""),
    }
}


# Password validation

AUTH_PASSWORD_VALIDATORS = [
    {"NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"},
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
        "OPTIONS": {"min_length": 10},
    },
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]


# Internationalization

LANGUAGE_CODE = "sk"

LANGUAGES = [("sk", "Slovensky"), ("cs", "Česky"), ("en", "English")]

LOCALE_PATHS = [BASE_DIR / "locales"]

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files

STATIC_ROOT = env("DJANGO_STATIC_ROOT", BASE_DIR / "static")

STATIC_URL = env("DJANGO_STATIC_URL", "/static/")

STATICFILES_DIRS = [BASE_DIR / "resources" / "public"]


# Logging

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": " ".join(
                [
                    "%(levelname)s",
                    "%(asctime)s",
                    "%(name)s.%(funcName)s():%(lineno)d",
                    "%(message)s",
                ]
            )
        },
        "areas": {"format": "** AREAS ** %(levelname)s %(asctime)s %(message)s"},
        "accounts": {"format": "** ACCOUNTS ** %(levelname)s %(asctime)s %(message)s"},
        "django.server": DEFAULT_LOGGING["formatters"]["django.server"],
    },
    "handlers": {
        "console_verbose": {"class": "logging.StreamHandler", "formatter": "verbose"},
        "console_areas": {"class": "logging.StreamHandler", "formatter": "areas"},
        "console_accounts": {"class": "logging.StreamHandler", "formatter": "accounts"},
        "django.server": DEFAULT_LOGGING["handlers"]["django.server"],
    },
    "loggers": {
        "root": {"handlers": ["console_verbose"], "level": "WARNING"},
        "areas": {"handlers": ["console_areas"], "level": "INFO", "propagate": False},
        "accounts": {
            "handlers": ["console_accounts"],
            "level": "INFO",
            "propagate": False,
        },
        "django.server": DEFAULT_LOGGING["loggers"]["django.server"],
    },
}


# Email

EMAIL_HOST = env("EMAIL_HOST")

EMAIL_PORT = env("EMAIL_PORT")

EMAIL_HOST_USER = env("EMAIL_HOST_USER")

EMAIL_HOST_PASSWORD = env("EMAIL_HOST_PASSWORD")

SIGNUP_EMAIL = f"signup@{PROJECT_DOMAIN}"

SERVER_EMAIL = f"server@{PROJECT_DOMAIN}"

DEFAULT_FROM_EMAIL = SERVER_EMAIL

EMAIL_SUBJECT_PREFIX = "[ISOPP] "

ADMINS = [("Ondřej Nejedlý", "jsmesami@gmail.com")]

MANAGERS = ADMINS + [("Ivan Iľko", "info@zonybezpesticidov.sk")]


# Security

SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

SECURE_SSL_REDIRECT = True

SECURE_BROWSER_XSS_FILTER = True

SESSION_COOKIE_SECURE = True

CSRF_COOKIE_SECURE = True


# Messages

MESSAGE_STORAGE = "django.contrib.messages.storage.session.SessionStorage"

MESSAGE_TAGS = {
    messages.SUCCESS: "success alert-success",
    messages.INFO: "info alert-info",
    messages.DEBUG: "debug alert-info",
    messages.WARNING: "warning alert-warning",
    messages.ERROR: "error alert-danger",
}


# Leaflet

LEAFLET_CONFIG = {
    "DEFAULT_CENTER": (48.6943302, 19.6994499),
    "DEFAULT_ZOOM": 8,
    "MIN_ZOOM": 4,
    "MAX_ZOOM": 19,
    "RESET_VIEW": False,
    "PLUGINS": {
        "leaflet-hash": {"js": "leaflet/leaflet-hash.js", "auto-include": True},
        "leaflet-locate": {
            "js": "leaflet/locate/L.Control.Locate.min.js",
            "auto-include": True,
        },
        "esri-leaflet": {"js": "leaflet/geocode/esri-leaflet.js", "auto-include": True},
        "esri-leaflet-geocoder": {
            "css": "leaflet/geocode/esri-leaflet-geocoder.css",
            "js": "leaflet/geocode/esri-leaflet-geocoder.js",
            "auto-include": True,
        },
    },
}

# Bootstrap4

BOOTSTRAP4 = {
    "formset_renderers": {
        "default": "core.renderers.FormsetRenderer",
    },
}

# App config

DANGER_BEFORE_SPRAYING = timedelta(days=1)  # Area is active n days before spraying starts

DANGER_AFTER_SPRAYING = timedelta(days=7)  # Area is active n days after spraying ends

MAX_SHOWN_INACTIVE_AREAS = 1000

# Misc config

MAPOTIC_ID = 743890

AUTH_USER_MODEL = "accounts.User"

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

LOGIN_URL = reverse_lazy("accounts:sign-in")

LOGIN_REDIRECT_URL = "home"

LOGOUT_REDIRECT_URL = "home"

SERIALIZATION_MODULES = {"geojson": "django.contrib.gis.serializers.geojson"}

MAINTENANCE_MODE = env("MAINTENANCE_MODE", False)
