import logging

from django import forms
from django.conf import settings
from django.contrib.auth import password_validation
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import gettext_lazy as _
from phonenumber_field.formfields import PhoneNumberField

from .models import User

logger = logging.getLogger(__name__)


class UserCreationAdminForm(UserCreationForm):
    class Meta:
        model = User
        fields = ("email",)


class UserChangeAdminForm(UserChangeForm):
    class Meta:
        model = User
        fields = "__all__"


class SignupForm(forms.ModelForm):
    first_name = forms.CharField(label=_("First name"), max_length=30, required=True)
    last_name = forms.CharField(label=_("Last name"), max_length=150, required=True)
    organization = forms.CharField(label=_("Organization"), required=True)
    phone = PhoneNumberField(label=_("Phone"), required=True)
    email = forms.EmailField(label=_("Email address"), required=True)

    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput,
        help_text=password_validation.password_validators_help_text_html(),
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super().__init__(*args, **kwargs)

    def clean_password(self):
        password = self.cleaned_data.get("password")
        if password:
            try:
                password_validation.validate_password(password, self.instance)
            except forms.ValidationError as error:
                self.add_error("password", error)
        return password

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        send_confirmation_email(self.request, user)
        return user

    class Meta:
        model = User
        fields = "first_name", "last_name", "organization", "phone", "email", "password"


def send_confirmation_email(request, user):
    current_site = get_current_site(request)
    subject = render_to_string("accounts/sign_up_email_subject.txt")
    message = render_to_string(
        "accounts/sign_up_email.txt",
        {
            "user": user,
            "protocol": "https" if request.is_secure() else "http",
            "domain": current_site.domain,
            "uid": urlsafe_base64_encode(force_bytes(user.pk)),
            "token": default_token_generator.make_token(user),
        },
    )
    user.email_user(subject, message, settings.SIGNUP_EMAIL)
    logger.info("Signup confirmation email has been send to %s.", user.email)
