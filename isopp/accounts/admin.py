import django.contrib.auth.admin as auth_admin
from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from .forms import UserChangeAdminForm, UserCreationAdminForm
from .models import User


class VerifiedFilter(admin.SimpleListFilter):

    title = _("is verified?")
    parameter_name = "is_verified"

    def lookups(self, request, model_admin):
        return ("1", _("Yes")), ("0", _("No"))

    def queryset(self, request, queryset):
        return {
            "0": queryset.exclude(groups__name="verified"),
            "1": queryset.filter(groups__name="verified"),
        }.get(self.value(), queryset)


@admin.register(User)
class UserAdmin(auth_admin.UserAdmin):

    model = User
    fieldsets = (
        (None, {"fields": ("email", "password")}),
        (
            _("Personal info"),
            {"fields": ("organization", "first_name", "last_name", "phone")},
        ),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_email_verified",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                )
            },
        ),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )
    add_fieldsets = (
        (None, {"classes": ("wide",), "fields": ("email", "password1", "password2")}),
    )
    form = UserChangeAdminForm
    add_form = UserCreationAdminForm
    search_fields = "organization", "first_name", "last_name", "email", "phone"
    list_filter = "is_active", "is_email_verified", VerifiedFilter, "is_staff"
    list_display = (
        "organization",
        "first_name",
        "last_name",
        "email",
        "phone",
        "is_active",
        "is_email_verified",
        "_is_verified",
        "is_staff",
    )
    ordering = ("-date_joined",)

    def _is_verified(self, obj):
        return obj.is_verified

    _is_verified.short_description = _("is verified?")
    _is_verified.boolean = True

    def has_delete_permission(self, request, obj=None):
        return False
