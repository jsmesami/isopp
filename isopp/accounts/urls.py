from django.contrib.auth.views import (
    LoginView,
    LogoutView,
    PasswordChangeDoneView,
    PasswordChangeView,
    PasswordResetCompleteView,
    PasswordResetConfirmView,
    PasswordResetDoneView,
    PasswordResetView,
)
from django.urls import path, reverse_lazy

from .views import (
    SignUpCompleteView,
    SignUpConfirmView,
    SignUpDoneView,
    SignUpVerifyView,
    SignUpView,
)

urlpatterns = [
    path("sign-up/", SignUpView.as_view(), name="sign-up"),
    path("sign-up/done/", SignUpDoneView.as_view(), name="sign-up-done"),
    path(
        "sign-up/verify/<int:user_id>/", SignUpVerifyView.as_view(), name="sign-up-verify"
    ),
    path(
        "sign-up/<uidb64>/<token>/", SignUpConfirmView.as_view(), name="sign-up-confirm"
    ),
    path("sign-up/complete/", SignUpCompleteView.as_view(), name="sign-up-complete"),
    path(
        "sign-in/",
        LoginView.as_view(template_name="accounts/sign_in.html"),
        name="sign-in",
    ),
    path(
        "sign-out/",
        LogoutView.as_view(template_name="accounts/signed_out.html"),
        name="sign-out",
    ),
    path(
        "password-change/",
        PasswordChangeView.as_view(
            template_name="accounts/password_change.html",
            success_url=reverse_lazy("accounts:password-change-done"),
        ),
        name="password-change",
    ),
    path(
        "password-change/done/",
        PasswordChangeDoneView.as_view(
            template_name="accounts/password_change_done.html"
        ),
        name="password-change-done",
    ),
    path(
        "password-reset/",
        PasswordResetView.as_view(
            template_name="accounts/password_reset.html",
            subject_template_name="accounts/password_reset_email_subject.txt",
            email_template_name="accounts/password_reset_email.txt",
            success_url=reverse_lazy("accounts:password-reset-done"),
        ),
        name="password-reset",
    ),
    path(
        "password-reset/done/",
        PasswordResetDoneView.as_view(template_name="accounts/password_reset_done.html"),
        name="password-reset-done",
    ),
    path(
        "password-reset/<uidb64>/<token>/",
        PasswordResetConfirmView.as_view(
            template_name="accounts/password_reset_confirm.html",
            success_url=reverse_lazy("accounts:password-reset-complete"),
        ),
        name="password-reset-confirm",
    ),
    path(
        "password-reset/complete/",
        PasswordResetCompleteView.as_view(
            template_name="accounts/password_reset_complete.html"
        ),
        name="password-reset-complete",
    ),
]
