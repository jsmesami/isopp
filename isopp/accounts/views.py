import logging

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.models import Group
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import mail_managers
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.utils.http import urlsafe_base64_decode
from django.utils.translation import gettext_lazy as _
from django.views import generic

from .forms import SignupForm
from .models import User

logger = logging.getLogger(__name__)


class SignUpView(generic.CreateView):

    form_class = SignupForm
    success_url = reverse_lazy("accounts:sign-up-done")
    template_name = "accounts/sign_up.html"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["request"] = self.request
        return kwargs

    def form_valid(self, form):
        logger.info(
            "User %s has successfully filled signup form.", form.cleaned_data["email"]
        )
        return super().form_valid(form)


class SignUpDoneView(generic.TemplateView):

    template_name = "accounts/sign_up_done.html"


INTERNAL_SIGNUP_SESSION_TOKEN = "_signup_token"
INTERNAL_SIGNUP_CONFIRMATION_TOKEN = "confirm"


class SignUpConfirmView(generic.View):

    template_name = "accounts/sign_up_confirm.html"
    http_method_names = ["get", "head", "options", "trace"]

    def get(self, request, uidb64, token):
        user = get_user(uidb64)

        if user:
            if token == INTERNAL_SIGNUP_CONFIRMATION_TOKEN:
                session_token = self.request.session.get(INTERNAL_SIGNUP_SESSION_TOKEN)
                if default_token_generator.check_token(user, session_token):
                    context = {
                        "link_valid": True,
                        "user": user,
                        "uidb64": uidb64,
                        "token": session_token,
                    }
                    logger.info("Signup confirmation form displayed for %s.", user.email)
                    return render(request, self.template_name, context)

            else:
                if default_token_generator.check_token(user, token):
                    request.session[INTERNAL_SIGNUP_SESSION_TOKEN] = token
                    redirect_url = request.path.replace(
                        token, INTERNAL_SIGNUP_CONFIRMATION_TOKEN
                    )
                    return HttpResponseRedirect(redirect_url)

        logger.warning(
            "Invalid signup confirmation displayed for user %s.", self.request.user.email
        )
        context = {"link_valid": False}
        return render(request, self.template_name, context)


def get_user(uidb64):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        return User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        return None


class SignUpCompleteView(generic.View):

    template_name = "accounts/sign_up_complete.html"

    def post(self, request):
        user = get_user(request.POST.get("uidb64"))
        token = request.POST.get("token")

        if user and default_token_generator.check_token(user, token):
            user.is_active = True
            user.is_email_verified = True
            user.save()
            logger.info("User % has been created.", user.email)
            login(request, user)
            send_verification_email(request, user)
            context = {"confirmation_valid": True, "user": user}
            return render(request, self.template_name, context)

        logger.warning("Invalid token or user during signup.")
        context = {"confirmation_valid": False}
        return render(request, self.template_name, context)

    def get(self, request):
        return redirect("home")


def send_verification_email(request, user):
    current_site = get_current_site(request)
    subject = render_to_string("accounts/sign_up_verify_email_subject.txt")
    message = render_to_string(
        "accounts/sign_up_verify_email.txt",
        {
            "user": user,
            "protocol": "https" if request.is_secure() else "http",
            "domain": current_site.domain,
        },
    )
    mail_managers(subject, message)
    logger.info("User verification link for %s has been sent to managers.", user.email)


class IsEditorMixin(UserPassesTestMixin):
    def test_func(self):
        if self.request.user.groups.filter(name="editor").exists():
            return True
        else:
            logger.warning(
                "Access to signup verification form denied for %s.",
                self.request.user.email,
            )
            return False


class SignUpVerifyView(IsEditorMixin, generic.DetailView):

    template_name = "accounts/sign_up_verify.html"
    queryset = User.objects.active()
    context_object_name = "user"
    pk_url_kwarg = "user_id"

    def post(self, request, **kwargs):
        verified = request.POST.get("verified")
        declined = request.POST.get("declined")
        self.object = user = self.get_object()

        group = Group.objects.get(name="verified")

        if verified:
            user.groups.add(group)
            messages.success(
                request, _("User {email} has been verified.").format(email=user.email)
            )
            send_email_verified(request, user)
            logger.info("Verification for %s has been granted.", user.email)

        if declined:
            user.groups.remove(group)
            messages.warning(
                request, _("User {email} has been declined.").format(email=user.email)
            )
            logger.warning("Verification for % has been declined.", user.email)

        context = self.get_context_data()
        return self.render_to_response(context)


def send_email_verified(request, user):
    current_site = get_current_site(request)
    subject = render_to_string("accounts/sign_up_verified_email_subject.txt")
    message = render_to_string(
        "accounts/sign_up_verified_email.txt",
        {
            "user": user,
            "protocol": "https" if request.is_secure() else "http",
            "domain": current_site.domain,
        },
    )
    user.email_user(subject, message, settings.SIGNUP_EMAIL)
    logger.info("Verification notice for %s has been sent.", user.email)
