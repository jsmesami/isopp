from django.conf import settings
from django.contrib.gis.serializers.geojson import Serializer
from django.utils.formats import date_format
from django.utils.text import Truncator


class AreasSerializer(Serializer):
    def __init__(self, now):
        self.now = now
        super().__init__()

    def prepare_period(self, period):
        status = (
            "current"
            if period.since <= self.now <= period.until
            else "future"
            if self.now + settings.DANGER_BEFORE_SPRAYING > period.since > self.now
            else "past"
            if self.now - settings.DANGER_AFTER_SPRAYING < period.until < self.now
            else "inactive"
        )

        return {
            "since": date_format(period.since, "SHORT_DATETIME_FORMAT"),
            "until": date_format(period.until, "SHORT_DATETIME_FORMAT"),
            "exec": period.executor,
            "status": status,
        }

    def end_object(self, obj):
        periods = [self.prepare_period(period) for period in obj.filtered_periods]
        statuses = {p["status"] for p in periods}

        status = (
            "current"
            if "current" in statuses
            else "future"
            if "future" in statuses
            else "past"
            if "past" in statuses
            else "inactive"
        )

        self._current["author"] = {"org": obj.author.organization}
        self._current["district"] = obj.district.name
        self._current["description"] = Truncator(obj.description).words(30)
        self._current["periods"] = periods
        self._current["status"] = status

        super().end_object(obj)
