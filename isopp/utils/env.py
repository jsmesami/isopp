import ast
import os
from contextlib import suppress


class ImproperlyConfigured(Exception):
    pass


def env(key, default=None):

    value = os.environ.get(key, default)

    with suppress(SyntaxError, ValueError):
        return ast.literal_eval(value)

    if value is None:
        raise ImproperlyConfigured(f"Missing required environment variable '{key}'.")

    return value
