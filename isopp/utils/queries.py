from django.conf import settings
from django.db.models import Case, Count, When


def recent_periods_filter(now, prefix=""):
    return {
        f"{prefix}until__gte": now - settings.DANGER_AFTER_SPRAYING,
    }


def active_periods_filter(now, prefix=""):
    return {
        f"{prefix}since__lte": now + settings.DANGER_BEFORE_SPRAYING,
        f"{prefix}until__gte": now - settings.DANGER_AFTER_SPRAYING,
    }


def active_periods_counter(now):
    return {
        "active_periods_count": Count(
            Case(When(**active_periods_filter(now, prefix="periods__"), then=1))
        )
    }
