from django.urls import path

from .views import CreateView, DetailView, UpdateView

urlpatterns = [
    path("detail/<pk>/", DetailView.as_view(), name="detail"),
    path("update/<pk>/", UpdateView.as_view(), name="update"),
    path("create/", CreateView.as_view(), name="create"),
]
