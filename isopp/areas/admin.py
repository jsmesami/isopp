from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from leaflet.admin import LeafletGeoAdmin

from .models import Area, District, SprayingPeriod


class SprayingPeriodAdminInline(admin.TabularInline):
    model = SprayingPeriod
    readonly_fields = ("created", "modified")
    allow_add = True
    extra = 0
    ordering = ("-created",)


@admin.register(District)
class DistrictAdmin(admin.ModelAdmin):
    model = District
    list_display = ("name", "_areas_count")
    search_fields = ("name",)
    ordering = ("name",)

    def _areas_count(self, obj):
        return obj.areas.count()

    _areas_count.short_description = _("areas count")


@admin.register(Area)
class AreaAdmin(LeafletGeoAdmin):
    model = Area
    readonly_fields = ("created", "modified")
    list_display = ("_id_field", "author", "district", "is_deleted", "_periods_count")
    list_filter = ("is_deleted", "district")
    search_fields = (
        "id",
        "district__name" "author__organization",
        "author__username",
        "author__email",
        "author__phone",
    )
    inlines = (SprayingPeriodAdminInline,)
    ordering = ("-created",)

    def _id_field(self, obj):
        return _("Area") + f" #{obj.pk}"

    _id_field.short_description = "id"
    _id_field.admin_order_field = "id"

    def has_delete_permission(self, request, obj=None):
        return False

    def _periods_count(self, obj):
        return obj.periods.count()

    _periods_count.short_description = _("periods count")
