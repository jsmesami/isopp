from bootstrap_datepicker_plus.widgets import DateTimePickerInput
from django.forms import ModelForm, Textarea, inlineformset_factory
from django.utils.translation import gettext_lazy as _
from leaflet.forms.widgets import LeafletWidget

from .models import Area, SprayingPeriod


class AreaForm(ModelForm):
    class Meta:
        model = Area
        fields = ("boundary", "district", "description")
        widgets = {
            "description": Textarea(attrs={"rows": "2"}),
            "boundary": LeafletWidget(),
        }


class SprayingPeriodForm(ModelForm):
    def clean(self):
        cleaned_data = super().clean()
        since = cleaned_data.get("since")
        until = cleaned_data.get("until")

        if since and until and since >= until:
            self.add_error("since", _("'Since' must be lower than 'Until'."))
            self.add_error("until", _("'Until' must be greater than 'Since'."))

    class Meta:
        model = SprayingPeriod
        fields = ("pest_kind", "executor", "since", "until", "area")
        widgets = {
            "since": DateTimePickerInput(
                attrs={"placeholder": "YYYY-MM-DD HH:MM", "class": "period-since"},
                options={"format": "YYYY-MM-DD HH:mm"},
            ),
            "until": DateTimePickerInput(
                attrs={"placeholder": "YYYY-MM-DD HH:MM", "class": "period-until"},
                options={"format": "YYYY-MM-DD HH:mm"},
            ),
        }


SprayingPeriodFormSet = inlineformset_factory(
    Area, SprayingPeriod, form=SprayingPeriodForm, extra=1, can_delete=False
)
