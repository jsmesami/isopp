import settings
from django.contrib.gis.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from utils.models import TimeStampedModel
from utils.queries import active_periods_filter, recent_periods_filter


class District(models.Model):
    name = models.CharField(_("name"), max_length=255, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("district")
        verbose_name_plural = _("districts")


class AreaQS(models.QuerySet):
    def valid(self):
        return self.filter(author__is_active=True, is_deleted=False)


class Area(TimeStampedModel):
    boundary = models.MultiPolygonField(_("boundary"))

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("author"),
        related_name="areas",
        on_delete=models.PROTECT,
    )

    district = models.ForeignKey(
        District,
        verbose_name=_("district"),
        related_name="areas",
        on_delete=models.PROTECT,
    )

    description = models.TextField(
        _("description"),
        help_text=_("Optional item"),
        blank=True,
    )

    is_deleted = models.BooleanField(_("is deleted?"), default=False, db_index=True)

    objects = models.Manager.from_queryset(AreaQS)()

    def get_absolute_url(self):
        return reverse("areas:detail", args=[self.id])

    def __str__(self):
        return _("Area") + f" #{self.id}"

    class Meta:
        verbose_name = _("area")
        verbose_name_plural = _("areas")


class SprayingPeriodQS(models.QuerySet):
    def recent(self, now):
        return self.filter(**recent_periods_filter(now))

    def active(self, now):
        return self.filter(**active_periods_filter(now))

    def inactive(self, now):
        return self.exclude(**active_periods_filter(now))


class SprayingPeriod(TimeStampedModel):
    class PestKind(models.IntegerChoices):
        FUNGI = 100, _("fungi")
        ANIMALS = 200, _("animals")
        PLANTS = 300, _("plants")

    pest_kind = models.SmallIntegerField(
        _("pest kind"),
        help_text=_("The type of pests against which the spray is applied"),
        choices=PestKind.choices,
    )
    executor = models.CharField(_("executor"), max_length=255)
    since = models.DateTimeField(_("since"))
    until = models.DateTimeField(_("until"))
    area = models.ForeignKey(
        "Area", verbose_name=_("area"), related_name="periods", on_delete=models.PROTECT
    )

    objects = models.Manager.from_queryset(SprayingPeriodQS)()

    def __str__(self):
        return _("Period") + f" #{self.id}"

    class Meta:
        verbose_name = _("spraying period")
        verbose_name_plural = _("spraying periods")
