import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.serializers import serialize
from django.db import transaction
from django.http import Http404
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.views import generic
from utils.serializers import AreasSerializer

from .forms import AreaForm, SprayingPeriodFormSet
from .models import Area, SprayingPeriod

logger = logging.getLogger(__name__)


class DetailView(generic.DetailView):

    template_name = "areas/detail.html"

    def get_object(self, queryset=None):
        try:
            pk = int(self.kwargs.get("pk"))
            return Area.objects.valid().select_related("author", "district").get(pk=pk)
        except (Area.DoesNotExist, ValueError):
            raise Http404

    def get_context_data(self, **kwargs):
        now = timezone.now()
        periods_qs = SprayingPeriod.objects.filter(area=self.object)
        periods_qs = (
            periods_qs.recent(now)
            if self.request.user.pk == self.object.author_id
            else periods_qs.active(now)
        )
        periods = periods_qs.all()

        self.object.filtered_periods = periods

        return {
            **super().get_context_data(**kwargs),
            "json": {
                "areas": AreasSerializer(now).serialize(
                    [self.object], geometry_field="boundary", fields=("status",)
                )
            },
            "periods_current": [p for p in periods if p.until >= now >= p.since],
            "periods_past": [p for p in periods if p.until < now],
            "periods_future": [p for p in periods if p.since > now],
        }


class UpdateView(SuccessMessageMixin, LoginRequiredMixin, generic.UpdateView):

    form_class = AreaForm
    template_name = "areas/update.html"
    success_message = _("The area was successfully updated.")
    login_url = reverse_lazy("accounts:sign-in")

    def get_object(self, queryset=None):
        try:
            pk = int(self.kwargs.get("pk"))
            return Area.objects.valid().get(pk=pk, author=self.request.user)
        except (Area.DoesNotExist, ValueError):
            raise Http404

    def get_context_data(self, **kwargs):
        periods_qs = SprayingPeriod.objects.recent(timezone.now())
        return {
            **super().get_context_data(**kwargs),
            "json": {"areas": serialize("geojson", [self.object])},
            "periods": (
                SprayingPeriodFormSet(
                    self.request.POST, instance=self.object, queryset=periods_qs
                )
                if self.request.POST
                else SprayingPeriodFormSet(instance=self.object, queryset=periods_qs)
            ),
        }

    def form_valid(self, form):
        periods = self.get_context_data().get("periods")

        with transaction.atomic():
            if periods.is_valid():
                response = super().form_valid(form)
                periods.instance = self.object
                periods.save()
                logger.info("Area % has been updated.", self.object.pk)
                return response
            else:
                return self.form_invalid(form)


class CreateView(SuccessMessageMixin, LoginRequiredMixin, generic.CreateView):

    form_class = AreaForm
    template_name = "areas/create.html"
    success_message = _("The area was successfully created.")
    login_url = reverse_lazy("accounts:sign-in")

    def get_context_data(self, **kwargs):
        return {
            **super().get_context_data(**kwargs),
            "periods": SprayingPeriodFormSet(self.request.POST)
            if self.request.POST
            else SprayingPeriodFormSet(),
        }

    def form_valid(self, form):
        periods = self.get_context_data().get("periods")

        with transaction.atomic():
            if periods.is_valid():
                form.instance.author = self.request.user
                form.instance.is_deleted = False
                response = super().form_valid(form)
                periods.instance = self.object
                periods.save()
                logger.info(
                    "Area %s has been created by %s", self.object.pk, self.request.user
                )
                return response
            else:
                return self.form_invalid(form)
