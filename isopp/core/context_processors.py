from django.conf import settings


def config(_request):
    return {"MAINTENANCE_MODE": settings.MAINTENANCE_MODE}
