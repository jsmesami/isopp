from core.views import MapView, MyAccountView, plain_text_view
from django.conf import settings
from django.urls import include, path

urlpatterns = [
    path("", MapView.as_view(template_name="home.html"), name="home"),
    path(f"api/v1/maps/{settings.MAPOTIC_ID}/", include("isopp.api.urls")),
    path("about/", MapView.as_view(template_name="about.html"), name="about"),
    path("legend/", MapView.as_view(template_name="legend.html"), name="legend"),
    path("my-account/", MyAccountView.as_view(), name="my-account"),
    path("robots.txt", plain_text_view, {"template_name": "robots.txt"}),
    path(".well-known/security.txt", plain_text_view, {"template_name": "security.txt"}),
]
