from django.conf import settings
from django.http import HttpResponsePermanentRedirect


class WwwRedirectMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        return (
            HttpResponsePermanentRedirect(
                f"https://{settings.PROJECT_DOMAIN}{request.path}"
            )
            if request.get_host().startswith("www.")
            else self.get_response(request)
        )
