from areas.models import Area, SprayingPeriod
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Prefetch
from django.shortcuts import render
from django.utils import timezone
from django.views import generic
from utils.queries import active_periods_counter
from utils.serializers import AreasSerializer


def prefetch_periods(area_qs, period_qs):
    return area_qs.prefetch_related(
        Prefetch(
            "periods",
            queryset=period_qs,
            to_attr="filtered_periods",
        )
    )


class MapView(generic.TemplateView):
    def get_context_data(self, **kwargs):
        now = timezone.now()
        areas = Area.objects.valid().select_related("author", "district")
        active_periods = SprayingPeriod.objects.active(now).order_by("-since")
        inactive_periods = SprayingPeriod.objects.inactive(now)

        filter_ = self.request.GET.get("filter")
        if filter_ == "current":
            areas = prefetch_periods(areas, active_periods).filter(
                periods__since__lte=now, periods__until__gte=now
            )
        elif filter_ == "future":
            areas = prefetch_periods(areas, active_periods).filter(
                periods__since__lt=now + settings.DANGER_BEFORE_SPRAYING,
                periods__since__gt=now,
            )
        elif filter_ == "past":
            areas = prefetch_periods(areas, active_periods).filter(
                periods__until__gt=now - settings.DANGER_AFTER_SPRAYING,
                periods__until__lt=now,
            )
        elif filter_ == "inactive":
            areas = (
                prefetch_periods(areas, inactive_periods)
                .filter(
                    periods__until__lt=now - settings.DANGER_AFTER_SPRAYING,
                )
                .exclude(
                    periods__until__gte=now - settings.DANGER_AFTER_SPRAYING,
                )
                .distinct()
                .order_by("-modified")[: settings.MAX_SHOWN_INACTIVE_AREAS]
            )
        else:
            areas = (
                prefetch_periods(areas, active_periods)
                .annotate(**active_periods_counter(now))
                .filter(active_periods_count__gte=1)
            )

        return {
            **super().get_context_data(**kwargs),
            "json": {
                "areas": AreasSerializer(now).serialize(
                    areas,
                    geometry_field="boundary",
                    fields=("pk", "description", "author", "periods", "status"),
                )
            },
        }


class MyAccountView(LoginRequiredMixin, generic.TemplateView):

    template_name = "my_account.html"

    def get_context_data(self, **kwargs):
        now = timezone.now()
        areas = (
            Area.objects.valid()
            .filter(author_id=self.request.user.pk)
            .select_related("author", "district")
        )
        recent_periods = SprayingPeriod.objects.recent(now).order_by("-since")

        return {
            **super().get_context_data(**kwargs),
            "user": self.request.user,
            "json": {
                "areas": AreasSerializer(now).serialize(
                    prefetch_periods(areas, recent_periods),
                    geometry_field="boundary",
                    fields=("description", "pk", "status"),
                )
            },
        }


def plain_text_view(request, template_name):
    return render(request, template_name, content_type="text/plain")
