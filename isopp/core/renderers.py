import bootstrap4.renderers as renderers


class FormsetRenderer(renderers.FormsetRenderer):
    def render_form(self, form, **kwargs):
        form = super().render_form(form, **kwargs)
        return f'<div class="formset-form">{form}</div>'
