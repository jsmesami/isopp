from contextlib import suppress

from areas.models import Area, SprayingPeriod
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point
from rest_framework import serializers
from rest_framework.decorators import api_view
from rest_framework.generics import get_object_or_404
from rest_framework.relations import RelatedField, StringRelatedField
from rest_framework.response import Response
from rest_framework_gis.serializers import GeoFeatureModelSerializer


class AuthorField(RelatedField):
    def to_representation(self, value):
        return value.organization


class SprayingPeriodSerializer(serializers.ModelSerializer):
    pest_kind = serializers.CharField(source="get_pest_kind_display")

    class Meta:
        model = SprayingPeriod
        fields = ("id", "pest_kind", "executor", "since", "until")


class AreaSerializer(GeoFeatureModelSerializer):
    author = AuthorField(read_only=True)
    district = StringRelatedField()
    periods = SprayingPeriodSerializer(many=True, read_only=True)

    class Meta:
        model = Area
        geo_field = "boundary"
        id_field = False
        fields = ("id", "author", "district", "description", "periods", "created")


@api_view()
def areas_list(request):
    qs = (
        Area.objects.valid()
        .select_related("author", "district")
        .prefetch_related("periods")
    )
    params = request.query_params

    if center := params.get("center"):
        with suppress(ValueError):
            lat, lng = center.split("|")
            loc = Point(float(lat), float(lng), srid=4326)
            qs = qs.annotate(distance=Distance("boundary", loc)).order_by("distance")

    if limit := params.get("limit"):
        with suppress(ValueError):
            qs = qs[: int(limit)]

    if author := params.get("author"):
        with suppress(ValueError):
            qs = qs.filter(author__id=int(author))

    if district := params.get("district"):
        with suppress(ValueError):
            qs = qs.filter(district__id=int(district))

    return Response(AreaSerializer(area).data for area in qs)


@api_view()
def area_detail(request, pk):
    qs = (
        Area.objects.valid()
        .select_related("author", "district")
        .prefetch_related("periods")
    )
    area = get_object_or_404(qs, pk=pk)

    return Response(AreaSerializer(area).data)
