from django.urls import path, re_path

from .areas.views import area_detail, areas_list
from .views import api_handler_404

urlpatterns = [
    path("pois.geojson/", areas_list, name="pois"),
    path("public-pois/<pk>/", area_detail, name="poi-detail"),
    re_path(r"^.*", api_handler_404),
]
