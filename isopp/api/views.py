from django.utils.translation import gettext_lazy as _
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response


@api_view()
def api_handler_404(request, format=None):  # noqa: A002
    return Response(
        data=dict(detail=_("Not found")),
        status=status.HTTP_404_NOT_FOUND,
    )
