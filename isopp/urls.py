from django.conf import settings
from django.contrib import admin
from django.urls import include, path, re_path
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView

admin.sites.AdminSite.site_header = _("Administration")
admin.sites.AdminSite.site_title = _("Administration")
admin.sites.AdminSite.index_title = "ISOPP"


if settings.MAINTENANCE_MODE:
    urlpatterns = [
        re_path("/*", TemplateView.as_view(template_name="maintenance.html")),
    ]
else:
    urlpatterns = [
        path("", include("core.urls")),
        path("admin/", admin.site.urls),
        path("accounts/", include(("accounts.urls", "accounts"), namespace="accounts")),
        path("areas/", include(("areas.urls", "areas"), namespace="areas")),
        path("lang/", include("django.conf.urls.i18n")),
    ]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
        path("404/", TemplateView.as_view(template_name="404.html")),
        path("403/", TemplateView.as_view(template_name="403.html")),
        path("500/", TemplateView.as_view(template_name="500.html")),
    ] + urlpatterns
